# BW-Maas IaC

Compresses and encode index.html

```bash
gzip -c < index.html | base64
```