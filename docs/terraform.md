# Terraform

Zum Starten habe ich folgenes main.tf erstellt:

```tf
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "us-west-2"
}

data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "app_server" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"

  tags = {
    Name = "ExampleAppServerInstance"
  }
}
```

Für AWS habe ich die Credentionls an den folgenen Ort eingefügt:

![Screenshot AWS Access](assets/aws-access.png)

Terraform Init

![Screenshot Teraform init](assets/terraform-init.png)

Terraform Plan

![Screenshot Teraform Init](assets/terraform-plan.png)
![](assets/terraform-plan2.png)

Terraform Apply

![Screenshot Teraform Apply](assets/terraform-apply.png)