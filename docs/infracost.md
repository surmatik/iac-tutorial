# Infracost

Mit dem Tool infracost kann man die montalichen Kosten von seinem Terraform Projekt schätzen lassen. Dafür habe ich mir auf [infracost.io](https://dashboard.infracost.io/) einen Account gemacht, dieses mit meinem Gitlab Account verbunden. Danach kann im Tool mit Gitlab Reposiotry auswöhlen und welches Terraform Projekt. 

Danach sehe ich hier, das die Jokes DB auf AWS ca. 24 Dollar pro Monat kosten wird:

![Screenshot Infracost](assets/infracost.png)

Für die MariaDB fallen Kosten von ca. 15 Dollar an und für die EC2 Instanz 9 Dollar.