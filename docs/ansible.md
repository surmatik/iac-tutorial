# Ansible

Für Ansible habe ich folgene Config

```cfg
[defaults]
host_key_checking = no
inventory = hosts
```

Als Hosts habe ich im host File folgene definiert:
```
[web]
web1 ansible_host=192.168.1.100 ansible_user=ubuntu
web2 ansible_host=192.168.1.101 ansible_user=ubuntu

[proxy]
rpx1 ansible_host=192.168.1.100 ansible_user=root
```

und dazu noch folgene site.yml für nginx

```yml
---
- hosts: web
  gather_facts: yes
  become: yes

  tasks:
    - name: Install basic packages
      ansible.builtin.package:
        name:
          - nginx

```