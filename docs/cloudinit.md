# CloudInit

Um meine Webseite in einer VM im BBW Maas automatisch zu deployen, habe ich folgenden Cloud Init erstellt, welche ich auf der BBW Maas Seite einfügen kann.

```yml
#cloud-config
users:
  - default
  - name: lino
    sudo: ALL=(ALL) NOPASSWD:ALL
    shell: /bin/bash
    ssh_authorized_keys:
      - [HIER KOMMT DER SSH KEY REIN]

packages_update: true
packages_upgrade: false

packages:
  - nginx

write_files:
  - encoding: gzip+base64
    content: H4sIAKy5P2UAAzWOuw7CMAxF93yF+YKqu5WBh8SCYECqGFPlolp1moqEgb8nJcHLlXx9jsy74/Vwf9xONOWg1vA/4Lw1VIazZIW9QBbQrL/YQ9Iq0JQgGdzVE8NdxQyP0X8aPvX27FRjKfu2Wu0gqnMMAQu595M8XjRgbLZ1M1VDgbZ/vscvJG6mAAAA
    path: /var/www/html/index.html
    permissions: '0666'
```

Für den Inhalt des `index.html` wurde hier zuvor der Inhalt mit komprimiert und dann mit base64 codeiert. Dies kann mit folgenem Befehl gemacht werden:
```bash
gzip -c < index.html | base64
```

<br>

Diesen Code habe ich dann in das BBW Maas kopiert und daraus folgene VM generiert.

![Screenshot BBW Maas](assets/bbw-maas.png)

Wenn ich dann mit dem BBW Maas VPN verbunden mit, kann ich die IP Adresse der VM in meinem Browser eingebe und bekomme folgene Seite:

![Screenshot Webseite](assets/cloudinit-webseite.png)
