# BBW IaC Tutorial - Lino Bertschinger

| Links | &#x2001; |
| ------------------------------------------------------------ | --- |
| <img src="https://cloudinit.readthedocs.io/en/latest/_static/logo-dark-mode.png" alt="CloudInit" style="width:65px;" /> | [CloudInit](cloudinit) |
| <img src="https://cdn.icon-icons.com/icons2/2389/PNG/512/ansible_logo_icon_145495.png" alt="Ansible" style="width:60px;" /> | [Ansible](ansible)
| <img src="https://static-00.iconduck.com/assets.00/terraform-icon-1803x2048-hodrzd3t.png" alt="Terraform" style="width:60px;" /> | [Terraform](terraform) <br> [Terraform Jokes DB](terraform-jokesdb) <br> [Infracost](infracost)  |

<style>
.md-typeset__table {
   min-width: 100%;
}

.md-typeset table:not([class]) {
    display: table;
}
</style>